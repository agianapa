#include <cstdlib>    // For `EXIT_FAILURE' constant
#include <iostream>
#include <iomanip>    // For std::precision

#include <getopt.h>   // FIXME: make it portable
#include <math.h>

#include <QDataStream>
#include <QFile>

/* Some constants */
#define MAGIC_NUMBER  0xA0B0C0D0
#define PROT_VERSION  0x1

/* Function prototypes */
static void usage(const char *pname);

int main(int argc, char *argv[])
{
    QFile file;
    float duration, endTime, startTime, timeStep;
    float x, y, z, xR, yR, zR, t;
    quint32 i, cnt, magicNumber, numRecords, numReplays, protVersion;
    int opt;

    /* Parse arguments */
    endTime = startTime = 0;
    numReplays = 1;
    timeStep = 1;
    while ((opt = getopt(argc, argv, "f:s:e:t:r:")) != -1) {
        switch (opt) {
	case 'f':
	    file.setFileName(optarg);
	    break;
        case 's':
	    startTime = atof(optarg);
            break;
        case 'e':
	    endTime = atof(optarg);
            break;
	case 't':
	    timeStep = atof(optarg);
	    break;
	case 'r':
	    numReplays = atoi(optarg);
	    break;
        default:
            usage(argv[0]);
	    /* Never reached */
        }
    }

    qDebug("Start time = %f\tEnd time = %f\tTime step = %f\tReplays = %d",
	   startTime, endTime, timeStep, numReplays);

    // Validate input
    duration = numReplays * (endTime - startTime);
    if (duration <= 0) {
        qDebug("Duration must be positive");
        exit(EXIT_FAILURE);
    }

    // Open file for writing
    if (file.exists()) {
      qDebug("File already exists!");
      exit(EXIT_FAILURE);
    }

    file.open(QIODevice::WriteOnly);
    QDataStream out(&file);

    // Write a header with a "magic number" and a protocol version
    out << (quint32) MAGIC_NUMBER;
    out << (quint32) PROT_VERSION;
    out << (float)  duration;
    out << (quint32) (duration / timeStep);

    // Write actual data
    for (i = 0, cnt = 0; i < numReplays; i++) {
	for (t = startTime; t < endTime; t += timeStep, cnt++) {
	    out << (float) cnt * timeStep
		<< (float) (5.0 * t * t)    << 0.0f << 0.0f
		<< (float) (cos(t) * 180.0) << 0.0f << 0.0f;
	}
    }

    // Done -- close file
    file.close();

    // Open file for reading
    file.open(QIODevice::ReadOnly);
    QDataStream in(&file);

    // Read "magic number" and protocol version
    in >> magicNumber;
    if (magicNumber != MAGIC_NUMBER) {
	qDebug("Magic number mismatch\n");
	exit(EXIT_FAILURE);
    }

    in >> protVersion;
    if (protVersion != PROT_VERSION) {
        qDebug("Protocol version mismatch\n");
        exit(EXIT_FAILURE);
    }

    in >> duration;
    in >> numRecords;

    std::cout << "Magic number = 0x" << std::hex << magicNumber << "\n"
	      << "Protocol version = 0x" << protVersion << "\n"
	      << "Duration(sec) = " << std::dec << duration << "\n"
	      << "# of records = "  << numRecords << "\n";

    // Read data
    while (!in.atEnd()) {
	in >> t >> x >> y >> z >> xR >> yR >> zR;
	std::cout << std::setprecision(3) << std::fixed
                  <<"t = " << t
		  << "\tx = "  << x
                  << "\ty = "  << y
                  << "\tz = "  << z
                  << "\txR = " << xR
                  << "\tyR = " << yR
                  << "\tzR = " << zR << "\n";
    }

    // Done -- close file
    file.close();

    return EXIT_SUCCESS;
}

static void usage(const char *pname)
{
    qDebug("Usage: %s -f fileName -s startTime -e endTime -t timeStep"
	    " -r replays\n", pname);
    exit(EXIT_FAILURE);
}
