#include <iostream>
#include <QtGui>
#include <Qt>
#include <QKeyEvent>

#include "myglwidget.h"
#include "window.h"

Window::Window()
{
    std::cout << "Window() contructor\n";

    // Set parent's window minimum dimensions
    this->setMinimumHeight(300);
    this->setMinimumWidth(300);

    // Construct and display opengl widget
    // Widgets are invisible by default
    myglWidget = new MyGLWidget(this);
    myglWidget->show();

    // Connect signals here
    connect(this, SIGNAL(stacksChanged(int )),
            myglWidget, SLOT(setNumOfStacks(int )));
    connect(this, SIGNAL(slicesChanged(int )),
            myglWidget, SLOT(setNumOfSlices(int )));

    connect(this, SIGNAL(xAngleChanged(GLfloat )),
            myglWidget, SLOT(rotateX(GLfloat )));
    connect(this, SIGNAL(yAngleChanged(GLfloat )),
            myglWidget, SLOT(rotateY(GLfloat )));
    connect(this, SIGNAL(zAngleChanged(GLfloat )),
            myglWidget, SLOT(rotateZ(GLfloat )));
}

void Window::resizeEvent(QResizeEvent *e)
{
    std::cout << "window::resizeEvent()" << std::endl;

    // Resize opengl widget to cover the whole window
    myglWidget->setGeometry(0, 0, e->size().width(), e->size().height());
}

void Window::keyPressEvent(QKeyEvent *e)
{
    std::cout << "window::keyPressEvent()" << std::endl;

    // Dispatch key events
    switch(e->key()) {
    case '+':
        std::cout << "emit stacksChanged()\n";
        emit stacksChanged(1);
        break;
    case '-':
        std::cout << "emit stacksChanged()\n";
        emit stacksChanged(-1);
        break;

    case '*':
        std::cout << "emit slicesChanged()\n";
        emit slicesChanged(1);
        break;
    case '/':
        std::cout << "emit slicesChanged()\n";
        emit slicesChanged(-1);
        break;

    case Qt::Key_X:
        std::cout << "emit xAngleChanged()\n";
        emit xAngleChanged(10);
        break;
    case Qt::Key_Y:
        std::cout << "emit yAngleChanged()\n";
        emit yAngleChanged(10);
        break;
    case Qt::Key_Z:
        std::cout<< "emit zAngleChanged()\n";
        emit zAngleChanged(10);
        break;
    default:
        // For key events we don't process,
        // just call the base class handler implementation.
        std::cout << "key = " << e->key() << std::endl;
        QWidget::keyPressEvent(e);
    }
}
