#include <cstdlib>
#include <iostream>

#include "workerthread.h"

WorkerThread::WorkerThread(QObject *parent, QQueue<float> *pData)
    : QThread(parent)
{
    // Initialize random generator here
    srand(time(NULL));

    abort = false;
    prev = 0;
    this->pData = pData;
}

WorkerThread::~WorkerThread()
{
    mutex.lock();
    abort = true;
    condition.wakeOne();
    mutex.unlock();

    wait();
}

void WorkerThread::run(void)
{
    float r;

    // Loop
    while (!abort) {
        r = 3.0 * (float) rand() / RAND_MAX;
        r += prev;
        prev = r;
        //std::cout << "r = " << r << std::endl;
        mutex.lock();
        if (pData->size() > 100) {
            std::cout << "WorkerThread::run()\t[BLOCKED]\n";
            condition.wait(&mutex);
            mutex.unlock();
        }
        else {
            pData->enqueue(r);
            mutex.unlock();
        }
    }
}

void WorkerThread::setData(QQueue<float> *pData)
{
    this->pData = pData;
}
