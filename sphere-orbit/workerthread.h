#include <QMutex>
#include <QQueue>
#include <QThread>
#include <QWaitCondition>

class WorkerThread : public QThread
{
    Q_OBJECT

public:
    WorkerThread(QObject *parent = 0, QQueue<float> *pdata = NULL);
    ~WorkerThread();
    void setData(QQueue<float> *pData);
    QWaitCondition condition;

protected:
    void run();

private:
    QMutex mutex;
    QQueue<float> *pData;
    float prev;
    bool abort;
};

