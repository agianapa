#include <iostream>
#include <QKeyEvent>
#include <QtOpenGL>

#include <cmath>
#include "myglwidget.h"

MyGLWidget::MyGLWidget(QWidget *parent)
    : QGLWidget(parent)
{
    std::cout << "GLWidget() constructor\n";

    nStacks = 10;
    nSlices = 10;

    xRot = 0.0f;
    yRot = 0.0f;
    zRot = 0.0f;

    //
    thread.setData(&this->data);
    thread.start();

    // Setup timer
    connect(&timer, SIGNAL(timeout()), this, SLOT(pullData()));
    timer.start(10);

    this->setMinimumWidth(300);
    this->setMinimumHeight(300);
}

MyGLWidget::~MyGLWidget()
{
    std::cout << "~GLWidget() deconstructor\n";
}

void MyGLWidget::setNumOfStacks(int dir)
{
    std::cout << "myglWidget::setNumOfStacks()" << std::endl;

    nStacks += dir;
    if (nStacks < 4)
        nStacks = 3;

    makeCurrent();
    glClear(GL_COLOR_BUFFER_BIT);
    drawSphere(nStacks, nSlices, 0.5f);
    updateGL();
}

void MyGLWidget::setNumOfSlices(int dir)
{
    std::cout << "myglWidget::setNumOfSlices()" << std::endl;

    nSlices += dir;
    if (nSlices < 4)
        nSlices = 3;

    makeCurrent();
    glClear(GL_COLOR_BUFFER_BIT);
    drawSphere(nStacks, nSlices, 0.5f);
    updateGL();
}

void MyGLWidget::rotateX(GLfloat angle)
{
    std::cout << "MyGLWidget::rotateX()\n";

    xRot += angle;
    updateGL();
}

void MyGLWidget::rotateY(GLfloat angle)
{
    std::cout << "MyGLWidget::rotateY()\n";

    yRot += angle;
    updateGL();
}

void MyGLWidget::rotateZ(GLfloat angle)
{
    std::cout << "MyGLWidget::rotateZ()\n";

    zRot += angle;
    updateGL();
}

void MyGLWidget::initializeGL()
{
    std::cout << "MyGLWidget::initializeGL()\n";

    // Enable backface culling
    glEnable(GL_CULL_FACE);

    // Clear color is set to black
    glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
    glShadeModel(GL_SMOOTH);

    // Reset modelview matrix
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    // We don't need to call drawSphere() here,
    // since the resizeGL() will be called upon
    // the initial creation of the window.
}

void MyGLWidget::paintGL()
{
    std::cout << "MyGLWidget::paintGL()\n";

    // Clear scene
    glClear(GL_COLOR_BUFFER_BIT);

    // Reset modelview matrix
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    // Rotate appropriately
    glRotatef(xRot, 1.0f, 0.0f, 0.0f);
    glRotatef(yRot, 0.0f, 1.0f, 0.0f);
    glRotatef(zRot, 0.0f, 0.0f, 1.0f);

    // Draw x, y, z axis
    drawAxis();

    // Draw sphere
    drawSphere(nStacks, nSlices, 0.5f);

    glFlush();
}

void MyGLWidget::resizeGL(int width, int height)
{
    std::cout << "MyGLWidget::resizeGL()\n";
    std::cout << "New width = " << width
              << "\nNew height = " << height << "\n";

    // Update viewport to cover the whole screen
    glViewport(0, 0, width, height);

    // Reset projection matrix
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    //gluPerspective(45.0, (GLfloat)width/(GLfloat)height, 0.1f, 100.0f);

    // Reset modelview matrix
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
}

void MyGLWidget::drawAxis(void)
{
    // Draw axis
    glLineWidth(3);
    glEnable(GL_LINE_SMOOTH);
    glColor3f(0.0f, 1.0f, 0.0f);

    glBegin(GL_LINES);
        glVertex3f(0.0f, 0.0f, 0.0f);    // x axis
        glVertex3f(1.0f, 0.0f, 0.0f);

        glVertex3f(0.0f, 0.0f, 0.0f);    // y axis
        glVertex3f(0.0f, 1.0f, 0.0f);

        glVertex3f(0.0f, 0.0f, 0.0f);    // z axis
        glVertex3f(0.0f, 0.0f, 1.0f);
    glEnd();

    // Restore color and line width
    glColor3f(1.0f, 1.0f, 1.0f);
    glDisable(GL_LINE_SMOOTH);
    glLineWidth(1);
}

void MyGLWidget::drawSphere(GLuint nStacks, GLuint nSlices, GLfloat r)
{
    GLfloat theta, phi, x, y, z, stepTheta, stepPhi;

    stepTheta = M_PI / nStacks;
    stepPhi = 2 * M_PI / nSlices;

    for (theta = 0; theta <= M_PI; theta += stepTheta) {
        glBegin(GL_QUAD_STRIP);
        for (phi = 0; phi <= 2.0 * M_PI + 0.1; phi += stepPhi) {
            x = r * sin(theta) * cos(phi);
            y = r * sin(theta) * sin(phi);
            z = r * cos(theta);
            glVertex3f(x, y, z);

            x = r * sin(theta + stepTheta) * cos(phi);
            y = r * sin(theta + stepTheta) * sin(phi);
            z = r * cos(theta + stepTheta);
            glVertex3f(x, y, z);
        }
        glEnd();
    }

    // The boundary edges of the polygon are drawn as line segments.
    glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
}

void MyGLWidget::pullData(void)
{
    float r;

    mutex.lock();
    if (!data.isEmpty()) {
        r = data.dequeue();
        //std::cout << "r = " << r << std::endl;
        xRot = yRot = zRot = r;
    }
    else {
        mutex.unlock();
        std::cout << "MyGLWidget::pullData()\t[WAKING THREAD]\n";
        thread.condition.wakeOne();
        return;
    }
    mutex.unlock();
    updateGL();
}
