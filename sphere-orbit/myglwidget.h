#ifndef MYGLWIDGET_H
#define MYGLWIDGET_H

#include <QGLWidget>
#include <QKeyEvent>
#include <QMutex>
#include <QTimer>

#include "workerthread.h"

class MyGLWidget : public QGLWidget
{
    Q_OBJECT

public:
    MyGLWidget(QWidget *parent = 0);
    ~MyGLWidget();

public slots:
    void setNumOfStacks(int dir);
    void setNumOfSlices(int dir);
    void rotateX(GLfloat angle);
    void rotateY(GLfloat angle);
    void rotateZ(GLfloat angle);

private slots:
    void pullData(void);

protected:
    void initializeGL();
    void paintGL();
    void resizeGL(int width, int height);

private:
    WorkerThread thread;
    QQueue<float> data;
    QTimer timer;
    QMutex mutex;

    void drawAxis(void);
    void drawSphere(GLuint nStacks, GLuint nSlices, GLfloat radius);
    unsigned int nStacks;
    unsigned int nSlices;
    GLfloat xRot;
    GLfloat yRot;
    GLfloat zRot;
};

#endif    // MYGLWIDGET_H
