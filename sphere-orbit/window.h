#ifndef WINDOW_H
#define WINDOW_H

#include <QtOpenGL>
#include <QWidget>
#include <QKeyEvent>

class MyGLWidget;

class Window : public QWidget
{
    Q_OBJECT

public:
    Window();

protected:
    void resizeEvent(QResizeEvent *e);
    void keyPressEvent(QKeyEvent *e);

private:
    MyGLWidget *myglWidget;

signals:
    void stacksChanged(int dir);
    void slicesChanged(int dir);
    void xAngleChanged(GLfloat angle);
    void yAngleChanged(GLfloat angle);
    void zAngleChanged(GLfloat angle);
};

#endif
