#include <iostream>
#include <QKeyEvent>
#include <QtOpenGL>

#include <math.h>
#include "myglwidget.h"

MyGLWidget::MyGLWidget(QWidget *parent)
    : QGLWidget(parent)
{
    std::cout << "GLWidget() constructor\n";

    angle = 0;

    this->setMinimumWidth(300);
    this->setMinimumHeight(300);
}

MyGLWidget::~MyGLWidget()
{
    std::cout << "~GLWidget() deconstructor\n";
}

void MyGLWidget::setRotation(int dir)
{
    std::cout << "myglWidget::setRotation" << std::endl;

    angle += dir;
    updateGL();
}

void MyGLWidget::resizeWidget(QResizeEvent *e)
{
    resizeGL(e->size().width(), e->size().height());
}

void MyGLWidget::initializeGL()
{
    std::cout << "InitializeGL()\n";

    glClearColor(0.0, 0.0, 0.0, 0.0);
    glShadeModel(GL_FLAT);
}

void MyGLWidget::paintGL()
{
    std::cout << "paintGL()\n";

    glClear(GL_COLOR_BUFFER_BIT);

    glLoadIdentity();
    glRotatef(angle, 0.0, 0.0, 1.);

    glBegin(GL_TRIANGLES);
        glVertex3f(-0.5, -0.5, 0.0);
        glVertex3f( 0.5,  0.0, 0.0);
        glVertex3f( 0.0,  0.5, 0.0);
    glEnd();
    glFlush();
}

void MyGLWidget::resizeGL(int width, int height)
{
    std::cout << "New width = " << width
              << "\nNew height = " << height << std::endl;

    glViewport(0, 0, width, height);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
}
