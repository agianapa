#include <QApplication>

#include "window.h"

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);
    Window window;

    // Widgets are invisible by default.
    window.show();

    return app.exec();
}
