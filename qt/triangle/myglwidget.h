#ifndef MYGLWIDGET_H
#define MYGLWIDGET_H

#include <QGLWidget>
#include <QKeyEvent>

class MyGLWidget : public QGLWidget
{
    Q_OBJECT

public:
    MyGLWidget(QWidget *parent = 0);
    ~MyGLWidget();

public slots:
    void setRotation(int dir);
    void resizeWidget(QResizeEvent *e);

protected:
    void initializeGL();
    void paintGL();
    void resizeGL(int width, int height);

private:
    int angle;
};

#endif    // MYGLWIDGET
