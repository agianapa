#include <iostream>
#include <QApplication>
#include <QTimeLine>

class QMyApplication : public QApplication
{
Q_OBJECT

public:
    QMyApplication(int argc, char *argv[]);	  
public slots:
    void foo(int i);
    void fooV(qreal value);
private:
    QTimeLine tl;
    int fps;
    int d;
};
