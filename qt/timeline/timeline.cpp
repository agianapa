#include <iostream>
#include "timeline.hpp"

QMyApplication::QMyApplication(int argc, char *argv[])
    : QApplication(argc, argv)
{
    std::cout << "QMyApplication::QMyApplication()\n";

    fps = 20;
    d = 10;    // in seconds

    tl.setFrameRange(0, fps * d);
    tl.setUpdateInterval(1000 / fps);
    tl.setDuration(1000 * d);
    connect(&tl, SIGNAL(frameChanged(int)), this, SLOT(foo(int)));
    connect(&tl, SIGNAL(valueChanged(qreal)), this, SLOT(fooV(qreal)));

    tl.start();
}

void QMyApplication::foo(int i)
{
    std::cout << "QMyApplication::foo()\n";
    std::cout << "Frame = " << i << "\n";
}

void QMyApplication::fooV(qreal value)
{
    std::cout << "QMyApplication::fooV()\n";
    std::cout << "Value = " << value << "\n";
}
