#include <iostream>
#include <QPainter>
#include <QPixmap>
#include <QString>
#include <QTextStream>

#include "myrank.h"
#include "mytabreportdialog.h"

MyTabReportDialog::MyTabReportDialog(QWidget *parent)
    : QDialog(parent)
{
    std::cout << "MyTabReportDialog::MyTabReportDialog()\n";

    setupUi(this);

    // Connect signals
    connect(this->unitComboBox, SIGNAL(currentIndexChanged(int )),
	    this, SLOT(updateLineEdits()));

    connect(this->aggregateLeftDaysCheckBox, SIGNAL(stateChanged(int )),
	    this, SLOT(updateLineEdits()));

    //
    pixmapChanged = true;
}

MyTabReportDialog::~MyTabReportDialog()
{
    std::cout << "MyTabReportDialog::~MyTabReportDialog()\n";
}

void MyTabReportDialog::paintEvent(QPaintEvent *event)
{
    std::cout << "MyTabReportDialog::paintEvent()\n";

    // Redraw graphics only if the pixmaps have changed since last time
    if (pixmapChanged == true) {
	labelGraphics->setScaledContents(true);
	labelGraphics->setPixmap(m_piePixmap);

	labelRank->setScaledContents(true);
	labelRank->setPixmap(m_rankPixmap);

	pixmapChanged = false;
    }
}

void MyTabReportDialog::resizeEvent(QResizeEvent *event)
{
    std::cout << "MyTabReportDialog::resizeEvent()\n";

    emit redrawContent();
}

void MyTabReportDialog::drawMyPie(QPainter *painter)
{
    std::cout << "MyTabReportDialog::drawMyPie()\n";

    // Days remaining
    painter->setBrush(QBrush(Qt::red, Qt::SolidPattern));
    painter->drawPie(0, 0,
                     labelGraphics->size().width(),
                     labelGraphics->size().height(),
                     0,
                     ((float) m_daysLeft / m_daysTotal) * 16 * 360);

    // Days off
    painter->setBrush(QBrush(Qt::blue, Qt::SolidPattern));
    painter->drawPie(0, 0,
                     labelGraphics->size().width(),
                     labelGraphics->size().height(),
                     ((float) m_daysLeft / m_daysTotal) * 16 * 360,
                     ((float) m_daysOff / m_daysTotal) * 16 * 360);

    // Days that have been served
    painter->setBrush(QBrush(Qt::green, Qt::SolidPattern));
    painter->drawPie(0, 0,
                     labelGraphics->size().width(),
                     labelGraphics->size().height(),
                     ((float) (m_daysLeft + m_daysOff) / m_daysTotal) * 16 * 360,
                     (((float) (m_daysServed) / m_daysTotal)) * 16 * 360);
}

void MyTabReportDialog::drawMyLegend(QPainter *painter)
{
    QString resultStr;
    float percentDaysServed;
    float percentDaysLeft;
    float percentDaysOff;

    std::cout << "MyTabReportDialog::drawMyLabel\n";

    // Calculate percentages
    percentDaysServed = 100.0 * m_daysServed / m_daysTotal;
    percentDaysLeft = 100.0 * m_daysLeft / m_daysTotal;
    percentDaysOff = 100.0 * m_daysOff / m_daysTotal;

    // Print percentages into a string using a text stream
    QTextStream out(&resultStr);
    out.setRealNumberPrecision(3);

    out << QString::fromUtf8("Υπηρετήθηκε = ") << percentDaysServed << "%\n";
    out << QString::fromUtf8("Και σήμερα = ") << percentDaysLeft << "%\n";
    out << QString::fromUtf8("Άδεια = ") << percentDaysOff << "%\n";

    // Draw string
    painter->setPen(QPen(Qt::white));
    painter->drawText(QRect(0, 0,
                            labelGraphics->size().width(),
                            labelGraphics->size().height()),
		      Qt::AlignCenter, resultStr);
}

void MyTabReportDialog::drawMyRank(QPainter *painter)
{
    std::cout << "MyTabReportDialog::drawMyRank()\n";

    // Load rank image
    m_image = QImage(m_rank.getImageFileName());
    if (m_image.isNull()) {
        std::cout << "MyTabReportDialog::drawMyRank(): "
		  << "the loading of the image failed\n";
	return;
    }

    // Draw rank image
    painter->drawImage(QPoint((labelRank->size().width() - m_image.width()) / 2,
                              (labelRank->size().height() - m_image.height()) / 2),
                       m_image);
}

void MyTabReportDialog::drawPixmaps(void)
{
    // Create the pixmaps
    m_piePixmap = QPixmap(labelGraphics->size());
    m_rankPixmap = QPixmap(labelRank->size());

    m_piePixmap.fill(this, 0, 0);
    m_rankPixmap.fill(this, 0, 0);

    // Create a QPainter to draw on the pixmap
    QPainter piePainter(&m_piePixmap);
    QPainter rankPainter(&m_rankPixmap);

    // Set the painter's pen and backround to be the same as the parent window
    piePainter.initFrom(this);
    rankPainter.initFrom(this);

    // Enable antialiasing mode
    piePainter.setRenderHint(QPainter::Antialiasing, true);
    rankPainter.setRenderHint(QPainter::Antialiasing, true);

    // Draw pie
    drawMyPie(&piePainter);

    // Draw label
    drawMyLegend(&piePainter);

    // Draw rank image
    drawMyRank(&rankPainter);

    // This function won't invoke an immediate repaint,
    // instead it will schedule a paint event that will be processed,
    // whenever Qt returns to the main event loop.
    update();
}

void MyTabReportDialog::updateLineEdits(void)
{
    int currentComboIndex;

    std::cout << "MyTabReportDialog::updateLineEdits()\n";

    // Get current selection regarding time unit
    currentComboIndex = unitComboBox->currentIndex();
    if (currentComboIndex == -1) {
	// XXX: No current item is set or the combobox is empty
	return;
    }

    std::cout << "Current combo index = " << currentComboIndex << std::endl;

    // Update line edits
    totalLineEdit->setText(getStrTime(m_daysTotal, currentComboIndex));
    servedLineEdit->setText(getStrTime(m_daysServed, currentComboIndex));
    daysoffLineEdit->setText(getStrTime(m_daysOff, currentComboIndex));

    if (aggregateLeftDaysCheckBox->isChecked())
	leftLineEdit->setText(getStrTime(m_daysLeft + m_daysOff, currentComboIndex));
    else
	leftLineEdit->setText(getStrTime(m_daysLeft, currentComboIndex));
}

void MyTabReportDialog::setUser(MyUser myUser)
{
    std::cout << "MyTabReportDialog::setUser()\n";

    m_user = myUser;

    m_daysLeft = myUser.getDaysLeft();
    m_daysOff = myUser.getDaysOff();
    m_daysServed = myUser.getDaysServed();
    m_daysTotal = myUser.getDaysTotal();

    m_rank = myUser.getRank();

    std::cout << "daysLeft = " << m_daysLeft
	      << "\tdaysOff = " << m_daysOff
	      << "\tdaysServed = " << m_daysServed
	      << "\tdaysTotal = " << m_daysTotal << "\n";
}

QString getStrTime(int nDays, int toWhat)
{
    int ret;

    std::cout << "getTime():\tnDays = " << nDays << "\ttoWhat = " << toWhat;
    switch(toWhat) {
    case MONTHS:
	ret = nDays / 30;
	break;
    case WEEKS:
	ret = nDays / 7;
	break;
    case DAYS:
	ret = nDays;
	break;
    case HOURS:
	ret = nDays * 24;
	break;
   case MINUTES:
	ret = nDays * 24 * 60;
	break;
    case SECONDS:
	ret = nDays * 24 * 60 * 60;
	break;
    default:
	ret = -1;
    }

    std::cout << "\tret = " << ret << std::endl;

    return QString::number(ret);
}
