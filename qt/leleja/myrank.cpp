#include "myrank.h"

MyRank::MyRank()
{
}

MyRank::MyRank(const char *rankname, const char *filename)
{
    m_rankName = QString::fromUtf8(rankname);
    m_imageFileName = QString(filename);
}

MyRank::~MyRank()
{
}

QString MyRank::getRankName(void) const
{
    return m_rankName;
}

QString MyRank::getImageFileName(void) const
{
    return m_imageFileName;
}
