#include <iostream>
#include <QtGui>
#include <QTextStream>

#include "accountDialog.h"
#include "myrank.h"
#include "mytabreportdialog.h"
#include "myuser.h"
//#include "ui_accountDialog.h"
#include "ui_tabReportDialog.h"

AccountDialog::AccountDialog(QWidget *parent)
    : QDialog(parent)
{
    std::cout << "AccountDialog::AccountDialog()\n";

    // Setup user interface
    setupUi(this);

    //
    pTabReportDialog = new MyTabReportDialog;

    pInputDialog = new InputDialog;
    connect(pInputDialog, SIGNAL(userAdded(MyUser )),
	    this, SLOT(addUser(MyUser )));

    connect(userListWidget, SIGNAL(itemSelectionChanged()),
	    this, SLOT(enableMenu()));

    // Populate rank list with <rank, imagePath>  tuples
    populateRankList();
#ifdef DEBUG
    printRankList();
#endif
}

AccountDialog::~AccountDialog()
{
    std::cout << "AccountDialog::~AccountDialog()\n";

    delete pInputDialog;
    delete pTabReportDialog;
}

void AccountDialog::on_addUserButton_clicked()
{
    std::cout << "AccountDialog::on_addUserButton_clicked()\n";

    // Place input dialog in the right of account dialog.
    pInputDialog->move(pos().x() + frameGeometry().width(),
		       pos().y());
    pInputDialog->show();
}

void AccountDialog::on_delUserButton_clicked()
{
    QListWidgetItem *currentItem;
    // int row;

    std::cout << "AccountDialog::on_delUserButton_clicked()\n";

    // Get currently selected item
    currentItem = userListWidget->currentItem();

    std::cout << m_userHash.remove(currentItem->text()) << std::endl;
    if (currentItem == NULL) {
        std::cout << "None item was selected\n";
        return;
    }

    // Remove <key> from hash
    m_userHash.remove(currentItem->text());

    // row = userListWidget->row(currentItem);
    // userListWidget->takeItem(row);
    delete currentItem;

    // Remove the key from the hash table as well

    // Disable delete and show buttons if there are no items at all.
    if (userListWidget->count() == 0) {
	delUserButton->setDisabled(true);
	showUserButton->setDisabled(true);
    }
}

void AccountDialog::on_showUserButton_clicked()
{
    QDate dayinDate;
    QDate leleDate;
    QString strkey;
    MyUser myUser;

    std::cout << "AutoDialog::on_showUserButton_clicked()\n";

    // Get `MyUser' object
    strkey = userListWidget->currentItem()->text();
    myUser = m_userHash.value(strkey);

    // Pass parameters to report dialog
    pTabReportDialog->setUser(myUser);

    // Always redraw content
    emit pTabReportDialog->redrawContent();

    // Show report dialog
   if (!pTabReportDialog->isVisible())
        pTabReportDialog->show();
}

void AccountDialog::on_exitButton_clicked()
{
    // qApp is a global pointer referring to the unique application object.
    qApp->closeAllWindows();
}

void AccountDialog::addUser(MyUser myUser)
{
    std::cout << "AccountDialog::addUser()\n";

    // At this point we complete the initialization of object
    //
    int rI =  (m_myRankList.size() - 1) *
	(myUser.getDaysLeft() + myUser.getDaysOff()) / myUser.getDaysTotal();
    myUser.setRank(m_myRankList[rI]);

    // Insert object to hash table
    if (m_userHash.contains(myUser.getName())) {
	QMessageBox::warning(this, trUtf8("Μήνυμα"),
                             trUtf8("Υπάρχει ήδη προφίλ με αυτό το όνομα\n"),
                             QMessageBox::Ok);
	return;
    }
    m_userHash[myUser.getName()] = myUser;

    // Add entry in the end of the list widget
    userListWidget->addItem(myUser.getName());

#ifdef DEBUG
    QTextStream toErr(stderr);

    QHash<QString, MyUser>::const_iterator i = m_userHash.constBegin();
    while (i != m_userHash.constEnd()) {
	toErr << "key = " << i.key() << ": "
	      << "\tname = " << i.value().getName()
	      << "\tdaysOff = " << i.value().getDaysOff() << "\n";
	++i;
    }
#endif
}

void AccountDialog::enableMenu(void)
{
  std::cout << "AccountDialog::enableMenu()\n";

  if (userListWidget->count() != 0 &&
      userListWidget->currentItem() != NULL) {
    showUserButton->setEnabled(true);
    delUserButton->setEnabled(true);
  }
  else {
    showUserButton->setEnabled(false);
    delUserButton->setEnabled(false);
  }
 
}

void AccountDialog::populateRankList(void)
{
    m_myRankList << MyRank("Στρατηγός", "./insignia/GR-Army-OF9.gif");
    m_myRankList << MyRank("Αντιστράτηγος", "./insignia/GR-Army-OF8.gif");
    m_myRankList << MyRank("Υποστράτηγος", "./insignia/GR-Army-OF7.gif");
    m_myRankList << MyRank("Ταξίαρχος", "./insignia/GR-Army-OF6.gif");

    m_myRankList << MyRank("Συνταγματάρχης", "./insignia/GR-Army-OF5.gif");
    m_myRankList << MyRank("Αντισυνταγματάρχης", "./insignia/GR-Army-OF4.gif");
    m_myRankList << MyRank("Ταγματάρχης", "./insignia/GR-Army-OF3.gif");

    m_myRankList << MyRank("Λοχαγός", "./insignia/GR-Army-OF2.gif");
    m_myRankList << MyRank("Υπολοχαγός", "./insignia/GR-Army-OF1a.gif");
    m_myRankList << MyRank("Ανθυπολοχαγός", "./insignia/GR-Army-OF1b.gif");
    m_myRankList << MyRank("Ανθυπασπιστής", "./insignia/GR-Army-WOa.gif");

    m_myRankList << MyRank("Δόκιμος", "./insignia/GR-Army-WOb.gif");
    m_myRankList << MyRank("Αρχιλοχίας", "./insignia/GR-Army-OR9.gif");
    m_myRankList << MyRank("Επιλοχίας", "./insignia/GR-Army-OR7.gif");
    m_myRankList << MyRank("Λοχίας", "./insignia/GR-Army-OR6.gif");
    m_myRankList << MyRank("Δεκανέας", "./insignia/GR-Army-OR4.gif");
    m_myRankList << MyRank("Υποδεκανέας", "./insignia/GR-Army-OR2a.gif");

    m_myRankList << MyRank("Στρατιώτης", "./insignia/fish.jpeg");
}

void AccountDialog::printRankList(void)
{
    int i;

    for (i = 0; i < m_myRankList.size(); i++) {
        qDebug() << "Rank = " << m_myRankList.at(i).getRankName() << "\t"
                 << "Image = " << m_myRankList.at(i).getImageFileName();
    }
}
