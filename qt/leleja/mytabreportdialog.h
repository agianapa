#ifndef MYTABREPORTDIALOG_H
#define MYTABREPORTDIALOG_H

#include <iostream>
#include <QDialog>
#include <QPixmap>

#include "myrank.h"
#include "myuser.h"
#include "ui_tabReportDialog.h"

#define MONTHS 0
#define WEEKS 1
#define DAYS 2
#define HOURS 3
#define MINUTES 4
#define SECONDS 5

class MyTabReportDialog : public QDialog, public Ui::tabReportDialog
{
    Q_OBJECT

public:
    MyTabReportDialog(QWidget *parent = 0);
    ~MyTabReportDialog();

    void setUser(MyUser myUser);

public slots:
    void redrawContent(void) {
        std::cout << "mytabreportdialog::redrawContent()\n";

        // Draw graphics in pixmaps
        drawPixmaps();
        pixmapChanged = true;

        // Update line edits
	updateLineEdits();
    }

private slots:
    void updateLineEdits(void);

protected:
    void paintEvent(QPaintEvent *event);
    void resizeEvent(QResizeEvent *event);

private:
    void drawMyLegend(QPainter *painter);
    void drawMyPie(QPainter *painter);
    void drawMyRank(QPainter *painter);
    void drawPixmaps(void);

    // Off screen image representation that can be used as a paint device
    QPixmap m_piePixmap;
    QPixmap m_rankPixmap;

    // Holds information regarding the rank (mark, name, etc)
    MyRank m_rank;

    // Indicates whether the pixmaps have changed,
    // since the last time they were drawn
    bool pixmapChanged;

    // Holds the rank image
    QImage m_image;

    // The various days parameters
    int m_daysLeft;
    int m_daysOff;
    int m_daysServed;
    int m_daysTotal;

    MyUser m_user;
};

QString getStrTime(int nDays, int toWhat);

#endif    // MYTABREPORTDIALOG_H
