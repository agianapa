<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS><TS version="1.1" language="en">
<defaultcodec></defaultcodec>
<context encoding="UTF-8">
    <name>AccountDialog</name>
    <message encoding="UTF-8">
        <location filename="accountDialog.cpp" line="129"/>
        <source>Μήνυμα</source>
        <translation>Message</translation>
    </message>
    <message encoding="UTF-8">
        <location filename="accountDialog.cpp" line="130"/>
        <source>Υπάρχει ήδη προφίλ με αυτό το όνομα
</source>
        <translation>There is already a profile with this name
</translation>
    </message>
</context>
<context encoding="UTF-8">
    <name>InputDialog</name>
    <message encoding="UTF-8">
        <location filename="inputDialog.cpp" line="56"/>
        <source>Μήνυμα</source>
        <translation>Message</translation>
    </message>
    <message encoding="UTF-8">
        <location filename="inputDialog.cpp" line="51"/>
        <source>Δεν έχετε καταταγεί στο στρατό ακόμη!
</source>
        <translation>You haven&apos;t joined the army yet!
</translation>
    </message>
    <message encoding="UTF-8">
        <location filename="inputDialog.cpp" line="57"/>
        <source>Είστε πολίτης!
</source>
        <translation>You are a citizen!
</translation>
    </message>
</context>
<context>
    <name>accountDialog</name>
    <message>
        <location filename="accountDialog.ui" line="13"/>
        <source>Leleja reloaded</source>
        <translation>Leleja reloaded</translation>
    </message>
    <message encoding="UTF-8">
        <location filename="accountDialog.ui" line="25"/>
        <source>Προφίλ χρηστών</source>
        <translation>Users&apos; profiles</translation>
    </message>
    <message encoding="UTF-8">
        <location filename="accountDialog.ui" line="46"/>
        <source>Νέος χρήστης</source>
        <translation>New user</translation>
    </message>
    <message encoding="UTF-8">
        <location filename="accountDialog.ui" line="65"/>
        <source>Διαγραφή</source>
        <translation>Delete</translation>
    </message>
    <message encoding="UTF-8">
        <location filename="accountDialog.ui" line="75"/>
        <source>Προβολή</source>
        <translation>Show</translation>
    </message>
    <message encoding="UTF-8">
        <location filename="accountDialog.ui" line="82"/>
        <source>Έξοδος</source>
        <translation>Exit</translation>
    </message>
</context>
<context>
    <name>inputDialog</name>
    <message encoding="UTF-8">
        <location filename="inputDialog.ui" line="13"/>
        <source>Εισαγωγή στοιχείων</source>
        <translation>Input data</translation>
    </message>
    <message encoding="UTF-8">
        <location filename="inputDialog.ui" line="30"/>
        <source>Όνομα χρήστη</source>
        <translation>Use name</translation>
    </message>
    <message encoding="UTF-8">
        <location filename="inputDialog.ui" line="37"/>
        <source>Ημερομηνία κατάταξης</source>
        <translation>Day you joined</translation>
    </message>
    <message encoding="UTF-8">
        <location filename="inputDialog.ui" line="44"/>
        <source>Ημερομηνία απόλυσης</source>
        <translation>Day you are dismissed</translation>
    </message>
    <message encoding="UTF-8">
        <location filename="inputDialog.ui" line="51"/>
        <source>Ημέρες άδειας</source>
        <translation>Days off</translation>
    </message>
    <message>
        <location filename="inputDialog.ui" line="88"/>
        <source>dd/MM/yyyy</source>
        <translation>dd/MM/yyyy</translation>
    </message>
    <message>
        <location filename="inputDialog.ui" line="98"/>
        <source>15</source>
        <translation>15</translation>
    </message>
    <message encoding="UTF-8">
        <location filename="inputDialog.ui" line="116"/>
        <source>Δημιουργία</source>
        <translation>Create</translation>
    </message>
</context>
<context encoding="UTF-8">
    <name>tabReportDialog</name>
    <message encoding="UTF-8">
        <location filename="tabReportDialog.ui" line="25"/>
        <source>Αναφορά</source>
        <translation>Report</translation>
    </message>
    <message encoding="UTF-8">
        <location filename="tabReportDialog.ui" line="41"/>
        <source>Αριθμητικά δεδομένα</source>
        <translation>Numerical data</translation>
    </message>
    <message encoding="UTF-8">
        <location filename="tabReportDialog.ui" line="53"/>
        <source>Στατιστικά</source>
        <translation>Statistics</translation>
    </message>
    <message encoding="UTF-8">
        <location filename="tabReportDialog.ui" line="68"/>
        <source>Υπηρετήθηκαν</source>
        <translation>Served</translation>
    </message>
    <message encoding="UTF-8">
        <location filename="tabReportDialog.ui" line="82"/>
        <source>Υπόλοιπο</source>
        <translation>Remaining</translation>
    </message>
    <message encoding="UTF-8">
        <location filename="tabReportDialog.ui" line="96"/>
        <source>Άδεια</source>
        <translation>Days off</translation>
    </message>
    <message encoding="UTF-8">
        <location filename="tabReportDialog.ui" line="117"/>
        <source>Σύνολο</source>
        <translation>Total</translation>
    </message>
    <message encoding="UTF-8">
        <location filename="tabReportDialog.ui" line="137"/>
        <source>Μονάδα μέτρησης</source>
        <translation>Measurement unit</translation>
    </message>
    <message encoding="UTF-8">
        <location filename="tabReportDialog.ui" line="145"/>
        <source>Μήνες</source>
        <translation>Months</translation>
    </message>
    <message encoding="UTF-8">
        <location filename="tabReportDialog.ui" line="150"/>
        <source>Εβδομάδες</source>
        <translation>Weeks</translation>
    </message>
    <message encoding="UTF-8">
        <location filename="tabReportDialog.ui" line="155"/>
        <source>Ημέρες</source>
        <translation>Days</translation>
    </message>
    <message encoding="UTF-8">
        <location filename="tabReportDialog.ui" line="160"/>
        <source>Ώρες</source>
        <translation>Hours</translation>
    </message>
    <message encoding="UTF-8">
        <location filename="tabReportDialog.ui" line="165"/>
        <source>Λεπτά</source>
        <translation>Minutes</translation>
    </message>
    <message encoding="UTF-8">
        <location filename="tabReportDialog.ui" line="170"/>
        <source>Δευτερόλεπτα</source>
        <translation>Seconds</translation>
    </message>
    <message encoding="UTF-8">
        <location filename="tabReportDialog.ui" line="187"/>
        <source>Να προσμετρώνται οι μέρες αδείας στο υπόλοιπο</source>
        <translation>Count days off in remaining days</translation>
    </message>
    <message encoding="UTF-8">
        <location filename="tabReportDialog.ui" line="193"/>
        <source>Γραφικά</source>
        <translation>Graphics</translation>
    </message>
    <message encoding="UTF-8">
        <location filename="tabReportDialog.ui" line="214"/>
        <source>Βαθμός</source>
        <translation>Rank</translation>
    </message>
</context>
</TS>
