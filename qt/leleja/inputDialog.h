#ifndef INPUT_DIALOG_H
#define INPUT_DIALOG_H

#include <QDialog>
#include <QList>

#include "myrank.h"
#include "mytabreportdialog.h"
#include "ui_inputDialog.h"
#include "myuser.h"

#define DEBUG

class InputDialog : public QDialog, Ui::inputDialog
{
  Q_OBJECT

public:
    InputDialog(QWidget *parent = 0);
    ~InputDialog();

signals:
    void userAdded(MyUser myUser);

private slots:
    void on_createButton_clicked();

private:
};

#endif    // INPUT_DIALOG_H
