#include "myuser.h"

MyUser::MyUser()
{
}

MyUser::MyUser(QString name, QDate dayInDate, QDate leleDate, int daysOff)
{
    m_name = name;
    m_dayInDate = dayInDate;
    m_leleDate = leleDate;
    m_daysOff = daysOff;

    //
    m_daysTotal = m_dayInDate.daysTo(m_leleDate);
    m_daysLeft = QDate::currentDate().daysTo(m_leleDate) - m_daysOff;
    m_daysServed = m_daysTotal - (m_daysLeft + m_daysOff);

    // Validate user input
    if (m_dayInDate > QDate::currentDate()) {
	throw 1;    // Not joined yet
    }

    if (m_daysLeft + m_daysOff < 1) {
        throw 2;    // Already dismissed
    }

    // At this point the object hasn't been fully initialized.
    // It still misses the rank object.
}


MyUser::~MyUser()
{
}

QString MyUser::getName(void) const
{
    return m_name;
}

QDate MyUser::getDayInDate(void) const
{
    return m_dayInDate;
}

QDate MyUser::getLeleDate(void) const
{
    return m_leleDate;
}

int MyUser::getDaysOff(void) const
{
    return m_daysOff;
}

//
int MyUser::getDaysLeft(void) const
{
    return m_daysLeft;
}

int MyUser::getDaysServed(void) const
{
    return m_daysServed;
}

int MyUser::getDaysTotal(void) const
{
    return m_daysTotal;
}

MyRank MyUser::getRank(void) const
{
    return m_rank;
}

void MyUser::setRank(MyRank rank)
{
    m_rank = rank;
}
