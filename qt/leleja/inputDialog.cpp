#include <iostream>
#include <QtGui>
#include <QTextStream>

#include "inputDialog.h"
#include "myrank.h"
#include "mytabreportdialog.h"

InputDialog::InputDialog(QWidget *parent)
    : QDialog(parent)
{
    std::cout << "InputDialog::InputDialog()\n";

    // Setup user interface
    setupUi(this);

    dayinDateEdit->setDate(QDate::currentDate());
    leleDateEdit->setDate(QDate::currentDate());
}

InputDialog::~InputDialog()
{
    std::cout << "InputDialog::~InputDialog()\n";
}

void InputDialog::on_createButton_clicked()
{
    QDate leleDate;
    QDate dayinDate;
    int daysOff;

    std::cout << "on_calcButton_clicked()\n";

    // Get dates
    dayinDate = dayinDateEdit->date();
    leleDate = leleDateEdit->date();
    daysOff = daysoffLineEdit->text().toInt();

    // Emit event
    // The other end is responsible for data validation
    try {
	emit userAdded(MyUser(userLineEdit->text(),
			      dayinDate,
			      leleDate,
			      daysOff));
    }
    catch(int x) {
	// Not joined yet
	if (x == 1) {
	    QMessageBox::warning(this, trUtf8("Μήνυμα"),
				 trUtf8("Δεν έχετε καταταγεί στο στρατό ακόμη!\n"),
				 QMessageBox::Ok);
	}
	// Already dismissed
	else if (x == 2) {
	    QMessageBox::warning(this, trUtf8("Μήνυμα"),
				 trUtf8("Είστε πολίτης!\n"),
				 QMessageBox::Ok);
	}
    }
}
