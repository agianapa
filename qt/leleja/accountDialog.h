#ifndef ACCOUNT_DIALOG_H
#define ACCOUNT_DIALOG_H

#include <QDialog>
#include <QList>
#include <QStringListModel>

#include "mytabreportdialog.h"
#include "ui_accountDialog.h"
#include "inputDialog.h"
#include "myuser.h"

#define DEBUG

class AccountDialog : public QDialog, Ui::accountDialog
{
  Q_OBJECT

public:
    AccountDialog(QWidget *parent = 0);
    ~AccountDialog();

private slots:
    void on_addUserButton_clicked();
    void on_delUserButton_clicked();
    void on_showUserButton_clicked();
    void on_exitButton_clicked();

    void addUser(MyUser myUser);
    void enableMenu(void);

private:
    void populateRankList(void);
    void printRankList(void);

    //
    InputDialog *pInputDialog;
    MyTabReportDialog *pTabReportDialog;

    // Rank list holds <rank, imagePath tuples>
    // (it's populated inside the leleja's constructor)
    QList<MyRank> m_myRankList;

    //
    QHash<QString, MyUser> m_userHash;
};

#endif    // ACCOUNT_DIALOG_H

