#ifndef MYRANK_H
#define MYRANK_H

#include <QString>

class MyRank {
public:
    MyRank();
    MyRank(const char *rankname, const char *filename);
    ~MyRank();

    QString getRankName(void) const;
    QString getImageFileName(void) const;
private:
    QString m_rankName;
    QString m_imageFileName;
};

#endif    // MYRANK_H
