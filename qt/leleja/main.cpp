#include <QApplication>
#include <QTextCodec>
#include <QTranslator>
#include <QDebug>

#include "accountDialog.h"

int main(int argc, char *argv[])
{
  QApplication app(argc, argv);

  // Set appropriate encoding for non Latin-1 strings
  QTextCodec::setCodecForTr(QTextCodec::codecForName("UTF-8"));

  // Load the message file (.qm) for the currently set locale.
  // If such a file doesn't exist, just fall back to the built in language.
  QTranslator myappTranslator;
  if (!myappTranslator.load("leleja_" + QLocale::system().name())) {
      qDebug() << "Error loading message file for:"
	       << QLocale::system().name() << "locale.";
      qDebug() << "Falling back to  built in language.";
  }
  app.installTranslator(&myappTranslator);

  AccountDialog dialog;
  dialog.show();

  return app.exec();
}
