#ifndef MYUSER_H
#define MYUSER_H

#include <QDate>
#include <QString>

#include "myrank.h"

class MyUser {
public:
    MyUser();
    MyUser(QString name, QDate dayInDate, QDate leleDate, int daysOff);
    ~MyUser();

    //
    QString getName(void) const;
    QDate getDayInDate(void) const;
    QDate getLeleDate(void) const;
    int getDaysOff(void) const;

    //
    int getDaysLeft(void) const;
    int getDaysServed(void) const;
    int getDaysTotal(void) const;

    //
    MyRank getRank(void) const;
    void setRank(MyRank rank);
private:
    QString m_name;
    QDate m_dayInDate;
    QDate m_leleDate ;
    int m_daysOff;
    int m_daysTotal;
    int m_daysLeft;
    int m_daysServed;
    MyRank m_rank;
};

#endif    // MYUSER_H
