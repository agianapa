#ifndef WINDOW_H
#define WINDOW_H

#include <QWidget>
#include <QKeyEvent>

class MyGLWidget;

class Window : public QWidget
{
    Q_OBJECT

public:
    Window();

protected:
    void resizeEvent(QResizeEvent *e);
    void keyPressEvent(QKeyEvent *e);

private:
    MyGLWidget *myglWidget;

signals:
    void windowResized(QResizeEvent *e);
    void rotationChanged(int dir);
    void zoomLevelChanged(int dir);
};

#endif
