#include <iostream>
#include <QKeyEvent>
#include <QtOpenGL>

#include <math.h>
#include "myglwidget.h"

MyGLWidget::MyGLWidget(QWidget *parent)
    : QGLWidget(parent)
{
    std::cout << "GLWidget() constructor\n";

    angle = 0;
    zoom = 0;

    this->setMinimumWidth(300);
    this->setMinimumHeight(300);
}

MyGLWidget::~MyGLWidget()
{
    std::cout << "~GLWidget() deconstructor\n";
}

void MyGLWidget::setRotation(int dir)
{
    std::cout << "MyGLWidget::setRotation()\n";

    angle += dir;
    updateGL();
}

void MyGLWidget::setZoom(int dir)
{
    std::cout << "MyGLWidget::setZoom()\t" << "dir = " << dir << "\n";

    zoom += dir;
    updateGL();
}

void MyGLWidget::resizeWidget(QResizeEvent *e)
{
    std::cout << "MyGLWidget::resizeWidget()\n";

    resizeGL(e->size().width(), e->size().height());
}

void MyGLWidget::initializeGL()
{
    std::cout << "InitializeGL()\n";

    glClearColor(0.0, 0.0, 0.0, 0.0);
    glShadeModel(GL_FLAT);
}

void MyGLWidget::paintGL()
{
    std::cout << "paintGL()\n";

    // The boundary edges of the polygon are drawn as line segments.
    //glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective(80.0f - zoom, 1.0f, 0.1f, 10.0f);

    //
    glMatrixMode(GL_MODELVIEW);
    glClear(GL_COLOR_BUFFER_BIT);
    glLoadIdentity();

    glRotatef(5 * angle, 0, 0, 1);

    // Draw triangle
    glBegin(GL_TRIANGLES);
        glVertex3f( -0.5, -0.5, -1.0);
        glVertex3f( 0.5, 0.0, -1.0);
        glVertex3f( 0.0, 0.5, -1.0);
    glEnd();

    //
    glFlush();
}

void MyGLWidget::resizeGL(int width, int height)
{
    std::cout << "MyGLWidget::resizeGL()";
    std::cout << "\twidth = " << width
              << "\theight = " << height << "\n";

    glViewport(0, 0, width, height);

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
}
