#include <QPushButton>
#include "exitbutton.hpp"

int main(int argc, char *argv[])
{
    QMyApplication myapp(argc, argv);
    QPushButton quit("quit");

    // http://doc.trolltech.com/4.4/signalsandslots.html#slots
    //
    // If several slots are connected to one signal, the slots will be
    // executed one after the other, in an arbitrary order, when the signal
    // is emitted.
    QObject::connect(&quit, SIGNAL(clicked()), &myapp, SLOT(foo()));
    QObject::connect(&quit, SIGNAL(clicked()), &myapp, SLOT(quit()));

    // By default a widget is not visible.
    quit.show();

    return myapp.exec();
}

