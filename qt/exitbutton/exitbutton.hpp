#include <iostream>
#include <QApplication>

class QMyApplication : public QApplication
{
Q_OBJECT

public:
    QMyApplication(int argc, char *argv[]) : QApplication(argc, argv)
    {
    }
public slots:
    void foo(void)
    {
        std::cout << "foo slot received a signal\n";
    }
};
