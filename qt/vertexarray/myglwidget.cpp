#include <iostream>
#include <QKeyEvent>
#include <QtOpenGL>

#include <cmath>
#include "myglwidget.h"

MyGLWidget::MyGLWidget(QWidget *parent)
    : QGLWidget(parent)
{
    std::cout << "GLWidget() constructor\n";

    numVertices = 10;
    pVertices = NULL;

    this->setMinimumWidth(300);
    this->setMinimumHeight(300);
}

MyGLWidget::~MyGLWidget()
{
    std::cout << "~GLWidget() deconstructor\n";

    delete[] pVertices;
}

void MyGLWidget::setNumOfVertices(int dir)
{
    std::cout << "myglWidget::setNumOfVertices()" << std::endl;

    numVertices += dir;
    if (numVertices > 2) {
        makeCurrent();
        glClear(GL_COLOR_BUFFER_BIT);
        drawCircle(numVertices);
        updateGL();
    }
    else
        numVertices = 2;
}

void MyGLWidget::initializeGL()
{
    std::cout << "InitializeGL()\n";

    glClearColor(0.0, 0.0, 0.0, 0.0);
    drawCircle(numVertices);
    glShadeModel(GL_FLAT);
}

void MyGLWidget::paintGL()
{
    std::cout << "paintGL()\n";

    glClear(GL_COLOR_BUFFER_BIT);
    glLoadIdentity();
    drawCircle(numVertices);
    glFlush();
}

void MyGLWidget::resizeGL(int width, int height)
{
    std::cout << "New width = " << width
              << "\nNew height = " << height << std::endl;

    glViewport(0, 0, width, height);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
}

void MyGLWidget::drawCircle(unsigned int numVertices)
{
    unsigned int i;

    std::cout << "n = " << numVertices << std::endl;

    if (pVertices)
        delete[] pVertices;

    pVertices = new GLfloat[2 * numVertices];

    // Populate the vertices array.
    for (i = 0; i < 2 * numVertices; i += 2) {
        pVertices[i  ] = std::cos((2 * M_PI / numVertices) * i / 2);
        pVertices[i+1] = std::sin((2 * M_PI / numVertices) * i / 2);
    }

    // The boundary edges of the polygon are drawn as line segments.
    glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

    // Activate and specify pointer to vertex array.
    glEnableClientState(GL_VERTEX_ARRAY);
    glVertexPointer(2, GL_FLOAT, 0, pVertices);

    // Draw polygon.
    glDrawArrays(GL_POLYGON, 0, numVertices);

    // Deactivate vertex arrays after drawing
    glDisableClientState(GL_VERTEX_ARRAY);

    glFlush();
}
