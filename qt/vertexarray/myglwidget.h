#ifndef MYGLWIDGET_H
#define MYGLWIDGET_H

#include <QGLWidget>
#include <QKeyEvent>

class MyGLWidget : public QGLWidget
{
    Q_OBJECT

public:
    MyGLWidget(QWidget *parent = 0);
    ~MyGLWidget();

public slots:
    void setNumOfVertices(int dir);

protected:
    void initializeGL();
    void paintGL();
    void resizeGL(int width, int height);

private:
    void drawCircle(unsigned int numVertices);
    unsigned int numVertices;
    GLuint mycircle;
    GLfloat *pVertices;
};

#endif    // MYGLWIDGET
