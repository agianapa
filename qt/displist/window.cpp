#include <iostream>
#include <QtGui>
#include <QKeyEvent>

#include "myglwidget.h"
#include "window.h"

Window::Window()
{
    std::cout << "Window() contructor\n";

    // Set parent's window minimum dimensions
    this->setMinimumHeight(300);
    this->setMinimumWidth(300);

    // Construct and display opengl widget
    // Widgets are invisible by default
    myglWidget = new MyGLWidget(this);
    myglWidget->show();

    // Connect signals here
    connect(this, SIGNAL(windowResized(QResizeEvent *)),
            myglWidget, SLOT(resizeWidget(QResizeEvent *)));
    connect(this, SIGNAL(verticesChanged(int )),
            myglWidget, SLOT(setNumOfVertices(int )));
}

void Window::resizeEvent(QResizeEvent *e)
{
    std::cout << "window::resizeEvent()" << std::endl;

    // Resize opengl widget to cover the whole window
    myglWidget->setGeometry(0, 0, e->size().width(), e->size().height());

    // Force opengl widget to redraw its contents
    emit windowResized(e);
}

void Window::keyPressEvent(QKeyEvent *e)
{
    std::cout << "window::keyPressEvent()" << std::endl;

    // Dispatch key events
    switch(e->key()) {
    case '+':
        emit verticesChanged(1);
        break;
    case '-':
        emit verticesChanged(-1);
        break;
    default:
        // For key events we don't process,
        // just call the base class handler implementation.
        QWidget::keyPressEvent(e);
    }
}
