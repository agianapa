#include <iostream>
#include <QKeyEvent>
#include <QtOpenGL>

#include <cmath>
#include "myglwidget.h"

MyGLWidget::MyGLWidget(QWidget *parent)
    : QGLWidget(parent)
{
    std::cout << "GLWidget() constructor\n";

    mycircle = 0;
    numVertices = 10;

    this->setMinimumWidth(300);
    this->setMinimumHeight(300);
}

MyGLWidget::~MyGLWidget()
{
    std::cout << "~GLWidget() deconstructor\n";

    makeCurrent();
    glDeleteLists(mycircle, 1);
}

void MyGLWidget::setNumOfVertices(int dir)
{
    std::cout << "myglWidget::setNumOfVertices" << std::endl;

    numVertices += dir;
    if (numVertices > 2) {
        makeCurrent();
        makeCircle(numVertices);
        updateGL();
    }
    else
        numVertices = 2;
}

void MyGLWidget::resizeWidget(QResizeEvent *e)
{
    resizeGL(e->size().width(), e->size().height());
}

void MyGLWidget::initializeGL()
{
    std::cout << "InitializeGL()\n";

    glClearColor(0.0, 0.0, 0.0, 0.0);
    mycircle = makeCircle(numVertices);
    glShadeModel(GL_SMOOTH);
}

void MyGLWidget::paintGL()
{
    std::cout << "paintGL()\n";

    glClear(GL_COLOR_BUFFER_BIT);

    glLoadIdentity();
    glCallList(mycircle);
    glFlush();
}

void MyGLWidget::resizeGL(int width, int height)
{
    std::cout << "New width = " << width
              << "\nNew height = " << height << std::endl;

    glViewport(0, 0, width, height);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
}

GLuint MyGLWidget::makeCircle(unsigned int numVertices)
{
    unsigned int i;
    GLuint mylist;

    std::cout << "n = " << numVertices << std::endl;

    // If there is already a display list, delete it
    // since we are going to repopulate it based on the
    // new number of vertices.
    if (mycircle)
        glDeleteLists(mycircle, 1);

    // Create display list.
    mylist = glGenLists(1);
    glNewList(mylist, GL_COMPILE);

    // The boundary edges of the polygon are drawn as line segments.
    glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

    // Populate the vertices.
    glBegin(GL_POLYGON);
    for (i = 0; i < numVertices; i++)
        glVertex2f(std::cos((2 * M_PI / numVertices) * i),
                   std::sin((2 * M_PI / numVertices) * i));
    glEnd();

    glEndList();
    return mylist;
}
