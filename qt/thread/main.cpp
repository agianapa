#include <QApplication>

#include "consumer.h"
#include "worker.h"

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);
    QQueue<int> datapool;

    WorkerThread worker(NULL, &datapool);
    ConsumerThread consumer(NULL, &datapool);

    worker.start();
    consumer.start();

    return app.exec();
}
