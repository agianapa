#include <QMutex>
#include <QQueue>
#include <QThread>

class WorkerThread : public QThread
{
    Q_OBJECT

public:
    WorkerThread(QObject *parent = 0, QQueue<int> *pdata = NULL);
    ~WorkerThread();

protected:
    void run();

private:
    QMutex mutex;
    QQueue<int> *pData;
};

