#include <cstdlib>
#include <iostream>

#include "worker.h"

WorkerThread::WorkerThread(QObject *parent, QQueue<int> *pData)
    : QThread(parent)
{
    // Initialize random generator here
    srand(time(NULL));

    this->pData = pData;
}

WorkerThread::~WorkerThread()
{
}

void WorkerThread::run(void)
{
    int r;

    // Loop forever
    for (;;) {
        r = rand();
        std::cout << "Worker: " << r << std::endl;
        mutex.lock();
        pData->enqueue(r);
        mutex.unlock();
    }
}

