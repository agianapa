#ifndef SPHERE_H
#define SPHERE_H

#include <QtOpenGL>
#include <cmath>

class Sphere {
public:
    Sphere(float radius = 0.5f, unsigned int slices = 20,
           unsigned int stacks = 20, bool hasCulling = true)
    {
        m_radius = radius;
        m_slices = slices;
        m_stacks = stacks;
        m_hasCulling = hasCulling;
    }
    ~Sphere() {};

    // Setters
    void setRadius(float radius) { m_radius = radius; }
    void setSlices(unsigned int slices) { m_slices = slices; }
    void setStacks(unsigned int stacks) { m_stacks = stacks; }
    void setCulling(bool hasCulling) { m_hasCulling = hasCulling; }

    // Getters
    float getRadius(void) const { return m_radius; }
    unsigned int getSlices(void) const { return m_slices; }
    unsigned int getStacks(void) const { return m_stacks; }
    bool getCulling(void) const { return m_hasCulling; }

    // Draw
    void draw(void) const
    {
        GLfloat theta, phi, r, x, y, z, stepTheta, stepPhi;

        qDebug("Sphere::draw()");
        qDebug("Stacks = %u\tSlices = %u\tr = %f", m_stacks, m_slices, m_radius);

        if (m_hasCulling)
            glEnable(GL_CULL_FACE);
        else
            glDisable(GL_CULL_FACE);

        stepTheta = M_PI / m_stacks;
        stepPhi = 2 * M_PI / m_slices;
        r = m_radius;

        for (theta = 0; theta <= M_PI; theta += stepTheta) {
            glBegin(GL_QUAD_STRIP);
            for (phi = 0; phi <= 2.0 * M_PI + 0.1; phi += stepPhi) {
                x = r * sin(theta) * cos(phi);
                y = r * sin(theta) * sin(phi);
                z = r * cos(theta);
                glVertex3f(x, y, z);

                x = r * sin(theta + stepTheta) * cos(phi);
                y = r * sin(theta + stepTheta) * sin(phi);
                z = r * cos(theta + stepTheta);
                glVertex3f(x, y, z);
            }
            glEnd();
        }

        // The boundary edges of the polygon are drawn as line segments.
        glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
    }

private:
    float m_radius;
    unsigned int m_slices;
    unsigned int m_stacks;
    bool m_hasCulling;
};

#endif    // SPHERE_H
