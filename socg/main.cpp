#include <QApplication>
#include "controlDialog.h"

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);
    ControlDialog controlDialog;

    // Widgets are invisible by default.
    // Window manager might ignore the following line.
    controlDialog.move(0, 0);
    controlDialog.show();

    return app.exec();
}
