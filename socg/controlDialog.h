#ifndef CONTROL_DIALOG_H
#define CONTROL_DIALOG_H

#include <iostream>
#include <QDialog>

#include "axis.h"
#include "camera.h"
#include "coordsAndAngles.h"
#include "sphere.h"
#include "myglwidget.h"
#include "workerthread.h"
#include "ui_controlDialog.h"

class WorkerThread;

class ControlDialog : public QDialog, public Ui::controlDialog
{
    Q_OBJECT

public:
    ControlDialog(QWidget *parent = 0);
    ~ControlDialog();

signals:
    void glSceneNeedsUpdate(void);
    void fileNameChanged(QString fileName);

private slots:
    void on_startButton_clicked();
    void on_stopButton_clicked();
    void on_exitButton_clicked();

    // Axis tab associated slots
    void on_xAxisVisibleCheck_stateChanged(int state);
    void on_yAxisVisibleCheck_stateChanged(int state);
    void on_zAxisVisibleCheck_stateChanged(int state);
    void on_localAxisCheck_stateChanged(int state);

    // Camera tab associated slots
    void on_eyeXEdit_textChanged(const QString& text);
    void on_eyeYEdit_textChanged(const QString& text);
    void on_eyeZEdit_textChanged(const QString& text);

    void on_centerXEdit_textChanged(const QString& text);
    void on_centerYEdit_textChanged(const QString& text);
    void on_centerZEdit_textChanged(const QString& text);

    void on_upXEdit_textChanged(const QString& text);
    void on_upYEdit_textChanged(const QString& text);
    void on_upZEdit_textChanged(const QString& text);

    // Sphere tab associated slots
    void on_stacksSpin_valueChanged(int i);
    void on_slicesSpin_valueChanged(int i);
    void on_radiusSpin_valueChanged(double d);
    void on_cullingCheck_stateChanged(int state);

    // Data tab associated slots
    void on_browseFileButton_clicked();
    void on_fileNameChangedManually();

    // Timeline tab associated slots
    void on_fpsSpin_valueChanged(int i);
    void pullData(qreal time);

private:
    MyGLWidget m_GLScene;
    WorkerThread m_thread;
    QMutex m_mutex;
    QTimeLine m_timeLine;

    Axis m_axisX;
    Axis m_axisY;
    Axis m_axisZ;
    bool m_localAxisSystem;

    Camera m_camera;
    Sphere m_sphere;
    CoordsAndAngles m_coordsAndAngles;
    
    float m_t;
};

#endif    // CONTROL_DIALOG_H
