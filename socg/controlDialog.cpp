#include <iostream>
#include <QTextStream>

#include "controlDialog.h"
#include "myglwidget.h"
#include "workerthread.h"

#define min(a, b) ((a) < (b) ? (a) : (b))

ControlDialog::ControlDialog(QWidget *parent)
    : QDialog(parent)
{
    qDebug("ControlDialog::ControlDialog()");

    // Worker thread populates the data pool with coordinations and angles.
    // It automatically blocks once the pool has reached a maximum number
    // of data and wakes up when data resource are low.
    m_thread.start();

    // Setup GUI
    setupUi(this);

    // Connect signals
    connect(this, SIGNAL(glSceneNeedsUpdate()),
            &m_GLScene, SLOT(updateGL()));
    connect(fileNameEdit, SIGNAL(returnPressed()),
            this, SLOT(on_fileNameChangedManually()));
    connect(this, SIGNAL(fileNameChanged(QString)),
            &m_thread, SLOT(setFileName(QString)));

    // Timeline
    connect(&m_timeLine, SIGNAL(valueChanged(qreal)),
            this, SLOT(pullData(qreal)));

    // Initialize axis, camera, sphere.
    // XXX: axis
    m_axisX.setColor(QColor(255, 0, 0));
    m_axisY.setColor(QColor(0, 255, 0));
    m_axisZ.setColor(QColor(0, 0, 255));
    m_localAxisSystem = false;

    // Camera
    m_camera.setEyeX(eyeXEdit->text().toFloat());
    m_camera.setEyeY(eyeYEdit->text().toFloat());
    m_camera.setEyeZ(eyeZEdit->text().toFloat());

    m_camera.setCenterX(centerXEdit->text().toFloat());
    m_camera.setCenterY(centerYEdit->text().toFloat());
    m_camera.setCenterZ(centerZEdit->text().toFloat());

    m_camera.setUpX(upXEdit->text().toFloat());
    m_camera.setUpY(upYEdit->text().toFloat());
    m_camera.setUpZ(upZEdit->text().toFloat());

   // Sphere
    m_sphere.setRadius(radiusSpin->value());
    m_sphere.setSlices(slicesSpin->value());
    m_sphere.setStacks(stacksSpin->value());
    m_sphere.setCulling(cullingCheck->checkState() == Qt::Checked ?
			true : false);

    // Pass objects to GL scene.
    m_GLScene.setAxis(AXIS_X, &m_axisX);
    m_GLScene.setAxis(AXIS_Y, &m_axisY);
    m_GLScene.setAxis(AXIS_Z, &m_axisZ);

    m_GLScene.setCamera(&m_camera);
    m_GLScene.setSphere(&m_sphere);
    m_GLScene.setCoordsAndAngles(&m_coordsAndAngles);

    // Show scene
    m_GLScene.setWindowTitle("GL Scene");
    // Window manager might ignore the following line
    m_GLScene.move(pos().x() + frameGeometry().width(), pos().y());
    m_GLScene.show();

    // At this point the animation isn't ready to begin.
    // We still miss the following function calls:
    // WorkerThread::setFileName()
    // QTimeLine::setFrameRange()
    // QTimeLine::setUpdateInterval()
    // QTimeLine::setDuration()

    // The value grows linearly (e.g., if the duration is 1000 ms,
    // the value at time 500 ms is 0.5). This isn't the default,
    // that's why we need to manually set it.
    m_timeLine.setCurveShape(QTimeLine::LinearCurve);
}

ControlDialog::~ControlDialog()
{
    qDebug("ControlDialog::~ControlDialog()");
}

void ControlDialog::on_startButton_clicked()
{
    qDebug("ControlDialog::on_startButton_clicked()");

    m_timeLine.start();
    stopButton->setEnabled(true);
}

void ControlDialog::on_stopButton_clicked()
{
    qDebug("ControlDialog::on_stopButton_clicked()");

    m_timeLine.stop();
    stopButton->setEnabled(false);
}

void ControlDialog::on_exitButton_clicked()
{
    // qApp is a global pointer referring to the unique application object.
    qApp->closeAllWindows();
}

void ControlDialog::on_xAxisVisibleCheck_stateChanged(int state)
{
    qDebug("ControlDialog::on_xAxisVisibleCheck_stateChanged()\n"
	   "New state = %d", state);

    m_axisX.setVisible(state == Qt::Checked ? true : false);
    emit glSceneNeedsUpdate();
}

void ControlDialog::on_yAxisVisibleCheck_stateChanged(int state)
{
    qDebug("ControlDialog::on_yAxisVisibleCheck_stateChanged()\n"
           "New state = %d", state);

    m_axisY.setVisible(state == Qt::Checked ? true : false);
    emit glSceneNeedsUpdate();
}

void ControlDialog::on_zAxisVisibleCheck_stateChanged(int state)
{
    qDebug("ControlDialog::on_zAxisVisibleCheck_stateChanged()\n"
           "New state = %d", state);

    m_axisZ.setVisible(state == Qt::Checked ? true : false);
    emit glSceneNeedsUpdate();
}

void ControlDialog::on_localAxisCheck_stateChanged(int state)
{
    qDebug("ControlDialog::on_localAxisCheck_stateChanged()\n"
           "New state = %d", state);

    m_axisX.setLocal(state == Qt::Checked ? true : false);
    m_axisY.setLocal(state == Qt::Checked ? true : false);
    m_axisZ.setLocal(state == Qt::Checked ? true : false);
    emit glSceneNeedsUpdate();
}

void ControlDialog::on_stacksSpin_valueChanged(int i)
{
    qDebug("ControlDialog::on_stacksSpin_valueChanged()\n"
           "New value = %d", i);

    m_sphere.setStacks(i);
    emit glSceneNeedsUpdate();
}

void ControlDialog::on_slicesSpin_valueChanged(int i)
{
    qDebug("ControlDialog::on_slicesSpin_valueChanged()\n"
           "New value = %d", i);

    m_sphere.setSlices(i);
    emit glSceneNeedsUpdate();
}

void ControlDialog::on_radiusSpin_valueChanged(double d)
{
    qDebug("ControlDialog::on_radiusSpin_valueChanged()\n"
	   "New value = %f", d);

    m_sphere.setRadius(d);
    emit glSceneNeedsUpdate();
}

void ControlDialog::on_cullingCheck_stateChanged(int state)
{
    qDebug("ControlDialog::on_cullingCheck_stateChanged()\n"
           "New state = %d", state);

    m_sphere.setCulling(state == Qt::Checked ? true : false);
    emit glSceneNeedsUpdate();
}

// Camera tab related slots
void ControlDialog::on_eyeXEdit_textChanged(const QString& text)
{
    qDebug("ControlDialog::on_eyeXEdit_textChanged()\n"
           "New eyeX = %f", text.toFloat());

    m_camera.setEyeX(text.toFloat());
    emit glSceneNeedsUpdate();
}

void ControlDialog::on_eyeYEdit_textChanged(const QString& text)
{
    qDebug("ControlDialog::on_eyeYEdit_textChanged()\n"
           "New eyeY = %f", text.toFloat());

    m_camera.setEyeY(text.toFloat());
    emit glSceneNeedsUpdate();
}

void ControlDialog::on_eyeZEdit_textChanged(const QString& text)
{
    qDebug("ControlDialog::on_eyeZEdit_textChanged()\n"
           "New eyeZ = %f", text.toFloat());

    m_camera.setEyeZ(text.toFloat());
    emit glSceneNeedsUpdate();
}

void ControlDialog::on_centerXEdit_textChanged(const QString& text)
{
    qDebug("ControlDialog::on_centerXEdit_textChanged()\n"
           "New centerX = %f", text.toFloat());

    m_camera.setCenterX(text.toFloat());
    emit glSceneNeedsUpdate();
}

void ControlDialog::on_centerYEdit_textChanged(const QString& text)
{
    qDebug("ControlDialog::on_centerYEdit_textChanged()\n"
           "New centerY = %f", text.toFloat());

    m_camera.setCenterY(text.toFloat());
    emit glSceneNeedsUpdate();
}

void ControlDialog::on_centerZEdit_textChanged(const QString& text)
{
    qDebug("ControlDialog::on_centerZEdit_textChanged()\n"
           "New centerZ = %f", text.toFloat());

    m_camera.setCenterZ(text.toFloat());
    emit glSceneNeedsUpdate();
}

void ControlDialog::on_upXEdit_textChanged(const QString& text)
{
    qDebug("ControlDialog::on_upXEdit_textChanged()\n"
           "New upX = %f", text.toFloat());

    m_camera.setUpX(text.toFloat());
    emit glSceneNeedsUpdate();
}

void ControlDialog::on_upYEdit_textChanged(const QString& text)
{
    qDebug("ControlDialog::on_upYEdit_textChanged()\n"
           "New upY = %f", text.toFloat());

    m_camera.setUpY(text.toFloat());
    emit glSceneNeedsUpdate();
}

void ControlDialog::on_upZEdit_textChanged(const QString& text)
{
    qDebug("ControlDialog::on_upZEdit_textChanged()\n"
           "New upZ = %f", text.toFloat());

    m_camera.setUpZ(text.toFloat());
    emit glSceneNeedsUpdate();
}

void ControlDialog::on_browseFileButton_clicked(void)
{
    QString fileName;
    float framesPerSec;
    qDebug("ControlDialog::on_browseFileButton_clicked()");

    // Get filename from open dialog
    fileName = QFileDialog::getOpenFileName(this, tr("Open data file"),
                                            "/home",
                                            "Data files(*.dat);; All files(*)");
    if (fileName == NULL) {
	qDebug("QFileDialog::getOpenFileName() returned NULL"
	       "(user pressed cancel"); 
	return;
    }

    // CAUTION: these lines must precede or else the
    // WorkerThread::extractHeader() won't be called.
    fileNameEdit->setText(fileName);
    emit fileNameChanged(fileName);

    // Update time line
    m_timeLine.setFrameRange(0, m_thread.getNumOfRecords());
    framesPerSec = min(fpsSpin->value(),
                       m_thread.getNumOfRecords() / m_thread.getDurationInSec());
    m_timeLine.setUpdateInterval(1000 / framesPerSec);
    m_timeLine.setDuration(m_thread.getDurationInSec() * 1000);
    qDebug("End frame: %d\n", m_timeLine.endFrame());

    // Construct strings based on the extracted header from the binary file
    QString magicVersionStr = QString("0x%1")
                              .arg(m_thread.getMagicVersion(), 0, 16).toUpper();
    QString protocolVersionStr = QString::number(m_thread.getProtocolVersion());
    QString durationStr = QString::number(m_thread.getDurationInSec());
    QString numberOfRecordsStr = QString::number(m_thread.getNumOfRecords());

    // Update the labels in control dialog
    magicVersionOutputLabel->setText(magicVersionStr);
    protocolVersionOutputLabel->setText(protocolVersionStr);
    durationOutputLabel->setText(durationStr);
    numberOfRecordsOutputLabel->setText(numberOfRecordsStr);

    if (!startButton->isEnabled())
        startButton->setEnabled(true);
}

void ControlDialog::on_fileNameChangedManually(void)
{
    qDebug("ControlDialog::on_fileNameChangedManually()");

    QString fileName = fileNameEdit->text();
    emit fileNameChanged(fileName);
}

void ControlDialog::pullData(qreal time)
{
    float t, xp, yp, zp, xa, ya, za;
    qDebug("ControlDialog::pullData()");

    if (m_t > time * m_thread.getDurationInSec())
      return;

    // Acquire lock
    m_thread.m_mutex.lock();

AGAIN:;
    qDebug("Timeline[0, 1] = %f\tTarget time(sec) = %f\tCurrent time(sec) = %f",
	   time, time * m_thread.getDurationInSec(), m_t);

    if (m_thread.m_data.size() >= 1000) {
        qDebug("Queue size = %d", m_thread.m_data.size());
	m_t = m_thread.m_data.dequeue();
        xp = m_thread.m_data.dequeue();
        yp = m_thread.m_data.dequeue();
        zp = m_thread.m_data.dequeue();
        xa = m_thread.m_data.dequeue();
        ya = m_thread.m_data.dequeue();
        za = m_thread.m_data.dequeue();
        //qDebug("t = %f\txp = %f\typ = %f\tzp = %f\txa = %f\tya = %f\tza = %f",
	//     t, xp, yp, zp, xa, ya, za);
        if (abs(m_t - m_thread.getDurationInSec() * time) < 0.01) {
	    m_coordsAndAngles.setCoordsAndAngles(xp, yp, zp, xa, ya, za);
	    goto OUT;
	}
        else {
            goto AGAIN;
        }
    }
    else {
        m_thread.m_mutex.unlock();
        qDebug("WAKING THREAD");
        m_thread.condition.wakeOne();
        return;
    }
OUT:;
    m_thread.m_mutex.unlock();

    emit glSceneNeedsUpdate();
}

void ControlDialog::on_fpsSpin_valueChanged(int i)
{
    float framesPerSec;
    qDebug("ControlDialog::on_fpsSpin_valueChanged()\n"
           "New value = %d", i);

    // Stop time line if it's up
    m_timeLine.stop();

    //
    framesPerSec = min(m_thread.getNumOfRecords() / m_thread.getDurationInSec(),
                       i);
    m_timeLine.setUpdateInterval(1000 / framesPerSec);

    // Don't resume animation, unless it was running before.
    if (stopButton->isEnabled())
	m_timeLine.resume();
}
