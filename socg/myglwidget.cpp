#include <iostream>
#include <QKeyEvent>
#include <QtOpenGL>

#include <cmath>

#include "axis.h"
#include "camera.h"
#include "coordsAndAngles.h"
#include "myglwidget.h"
#include "workerthread.h"

// Non-default arguments must come first
MyGLWidget::MyGLWidget(Axis *pAxisX, Axis *pAxisY, Axis *pAxisZ,
                       Camera *pCamera, Sphere * pSphere, QWidget *parent)
    : QGLWidget(parent)
{
    qDebug("MyGLWidget::MyGLWidget()");

    m_pAxisX = pAxisX;
    m_pAxisY = pAxisY;
    m_pAxisZ = pAxisZ;

    m_pCamera = pCamera;
    m_pSphere = pSphere;
}

MyGLWidget::~MyGLWidget()
{
    qDebug("MyGLWidget::~MyGLWidget()");
}

void MyGLWidget::initializeGL()
{
    qDebug("MyGLWidget::initializeGL()");

    // Enable backface culling
    glEnable(GL_CULL_FACE);

    // Clear color is set to black
    glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
    glShadeModel(GL_SMOOTH);

    // Reset modelview matrix
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    // We don't need to call drawSphere() here,
    // since the resizeGL() will be called upon
    // the initial creation of the window.
}

void MyGLWidget::paintGL()
{
    qDebug("MyGLWidget::paintGL()");

    // Clear scene
    glClear(GL_COLOR_BUFFER_BIT);

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective(90, 1, 0.01, 100);

    // Reset modelview matrix
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    //gluLookAt(1, 1, 1, 0, 0, -1, 0, 1, 0);
    gluLookAt(m_pCamera->getEyeX(),
	      m_pCamera->getEyeY(),
	      m_pCamera->getEyeZ(),
	      m_pCamera->getCenterX(),
	      m_pCamera->getCenterY(),
	      m_pCamera->getCenterZ(),
	      m_pCamera->getUpX(),
	      m_pCamera->getUpY(),
	      m_pCamera->getUpZ());

    // Draw global axis system
    m_pAxisX->draw(0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f);
    m_pAxisY->draw(0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f);
    m_pAxisZ->draw(0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f);

    // Rotate appropriately
    qDebug("xP = %f\tyP = %f\tzP = %f\txA = %f\tyA = %f\tzA = %f",
	   m_pCoordsAndAngles->getXPos(),
	   m_pCoordsAndAngles->getYPos(),
	   m_pCoordsAndAngles->getZPos(),
	   m_pCoordsAndAngles->getXAngle(),
	   m_pCoordsAndAngles->getYAngle(),
	   m_pCoordsAndAngles->getZAngle());

    glTranslatef(0, -m_pCoordsAndAngles->getXPos()/700, 0);

    glRotatef(m_pCoordsAndAngles->getXAngle(), 1.0f, 0.0f, 0.0f);
    glRotatef(m_pCoordsAndAngles->getYAngle(), 0.0f, 1.0f, 0.0f);
    glRotatef(m_pCoordsAndAngles->getZAngle(), 0.0f, 0.0f, 1.0f);

    // Local Axis
    if (m_pAxisX->isLocal() || m_pAxisY->isLocal() || m_pAxisZ->isLocal()) {
	m_pAxisX->draw(0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f);
	m_pAxisY->draw(0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f);
	m_pAxisZ->draw(0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f);
    }

    // Draw sphere
    m_pSphere->draw();

    glFlush();
}

void MyGLWidget::resizeGL(int width, int height)
{
    qDebug("MyGLWidget::resizeGL()\n"
	   "New width = %d\tNew height = %d", width, height);

    // Update viewport to cover the whole screen
    glViewport(0, 0, width, height);

    // Reset projection matrix
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    //gluPerspective(45.0, (GLfloat)width/(GLfloat)height, 0.1f, 100.0f);

    // Reset modelview matrix
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
}

void MyGLWidget::setAxis(axis_t axis, const Axis *pAxis)
{
    qDebug("MyGLWidget::setAxis()");

    switch(axis) {
    case AXIS_X:
        m_pAxisX = pAxis;
        break;
    case AXIS_Y:
        m_pAxisY = pAxis;
        break;
    case AXIS_Z:
        m_pAxisZ = pAxis;
        break;
    };
}

void MyGLWidget::setCamera(const Camera *pCamera)
{
    qDebug("MyGLWidget::setCamera()");

    m_pCamera = pCamera;
}

void MyGLWidget::setSphere(const Sphere *pSphere)
{
    qDebug("MyGLWidget::setSphere()");

    m_pSphere = pSphere;
}

void MyGLWidget::setCoordsAndAngles(const CoordsAndAngles *pCoordsAndAngles)
{
    qDebug("MyGLWidget::setCoordsAndAngles()");

    m_pCoordsAndAngles = pCoordsAndAngles;
}
