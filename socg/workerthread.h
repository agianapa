#ifndef WORKERTHREAD_H
#define WORKERTHREAD_H

#include <QDataStream>
#include <QFile>
#include <QMutex>
#include <QQueue>
#include <QThread>
#include <QWaitCondition>

class WorkerThread : public QThread
{
    Q_OBJECT

public:
    WorkerThread(QObject *parent = 0);
    ~WorkerThread();
    QWaitCondition condition;

    quint32 getMagicVersion(void) const;
    quint32 getProtocolVersion(void) const;
    float getDurationInSec(void) const;
    quint32 getNumOfRecords(void) const;

    friend class ControlDialog;

public slots:
    void setFileName(QString fileName);

protected:
    void run();

private:
    void extractFileHeader(void);

    QFile m_file;
    QDataStream in;
    QMutex m_mutex;
    QQueue<float> m_data;
    //float prev;
    bool m_abort;

    // Header specific
    quint32 m_magicVersion;
    quint32 m_protocolVersion;
    float  m_durationInSec;
    quint32 m_numOfRecords;
};

#endif    // WORKERTHREAD_H
