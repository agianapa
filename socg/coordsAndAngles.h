#ifndef COORDSANDANGLES_H
#define COORDSANDANGLES_H

class CoordsAndAngles {
public:

    CoordsAndAngles() {};
    CoordsAndAngles(float xPos, float yPos, float zPos,
                    float xAngle,  float yAngle, float zAngle)
    {
        m_xPos = xPos;
        m_yPos = yPos;
        m_zPos = zPos;

        m_xAngle = xAngle;
        m_yAngle = yAngle;
        m_zAngle = zAngle;
    }
    ~CoordsAndAngles() {};

    void setCoordsAndAngles(float xPos, float yPos, float zPos,
                            float xAngle,  float yAngle, float zAngle)
    {
        m_xPos = xPos;
        m_yPos = yPos;
        m_zPos = zPos;

        m_xAngle = xAngle;
        m_yAngle = yAngle;
        m_zAngle = zAngle;
    }

    float getXPos(void) const { return m_xPos; }
    float getYPos(void) const { return m_yPos; }
    float getZPos(void) const { return m_zPos; }

    float getXAngle(void) const { return m_xAngle; }
    float getYAngle(void) const { return m_yAngle; }
    float getZAngle(void) const { return m_zAngle; }

private:
    // Position
    float m_xPos;
    float m_yPos;
    float m_zPos;
    // Orientation (angles)
    float m_xAngle;
    float m_yAngle;
    float m_zAngle;
};

#endif    // COORDSANDANGLES
