#ifndef CAMERA_H
#define CAMERA_H

class Camera {
public:
	Camera() {};
	Camera(float eyeX, float eyeY, float eyeZ,
	    float centerX, float centerY, float centerZ,
	    float upX, float upY, float upZ)
	     {
	        m_eyeX = eyeX;
	        m_eyeY = eyeY;
	        m_eyeZ = eyeZ;

	        m_centerX = centerX;
	        m_centerY = centerY;
	        m_centerZ = centerZ;

	        m_upX = upX;
	        m_upY = upY;
	        m_upZ = upZ;
	     }
	~Camera() {};

    // Setters
	void setEyeX(float eyeX) { m_eyeX = eyeX; }
	void setEyeY(float eyeY) { m_eyeY = eyeY; }
	void setEyeZ(float eyeZ) { m_eyeZ = eyeZ; }

	void setCenterX(float centerX) { m_centerX = centerX; }
	void setCenterY(float centerY) { m_centerY = centerY; }
	void setCenterZ(float centerZ) { m_centerZ = centerZ; }

	void setUpX(float upX) { m_upX = upX; }
	void setUpY(float upY) { m_upY = upY; }
	void setUpZ(float upZ) { m_upZ = upZ; }

	// Getters
	float getEyeX(void) const { return m_eyeX; }
	float getEyeY(void) const { return m_eyeY; }
	float getEyeZ(void) const { return m_eyeZ; }

	float getCenterX(void) const { return m_centerX; }
	float getCenterY(void) const { return m_centerY; }
	float getCenterZ(void) const { return m_centerZ; }

	float getUpX(void) const { return m_upX; }
	float getUpY(void) const { return m_upY; }
	float getUpZ(void) const { return m_upZ; }
private:
	float m_eyeX;
	float m_eyeY;
	float m_eyeZ;

	float m_centerX;
	float m_centerY;
	float m_centerZ;

	float m_upX;
	float m_upY;
	float m_upZ;
};

#endif    // CAMERA_H
