#include <cstdlib>
#include <iostream>
#include <QDebug>

#include "workerthread.h"

WorkerThread::WorkerThread(QObject *parent)
    : QThread(parent)
{
    qDebug("WorkerThread::WorkerThread()");

    // Initialize random generator here
    srand(time(NULL));

    // Make thread work forever by default
    m_abort = false;

    // At this point the class hasn't been fully initialized.
    // The user must call setFileName(), before run()'ing.
}

WorkerThread::~WorkerThread()
{
    qDebug("WorkerThread::~WorkerThread()");

    m_mutex.lock();
    m_abort = true;
    condition.wakeOne();
    m_mutex.unlock();

    m_file.close();

    wait();
}

void WorkerThread::run(void)
{
    float r;
    qDebug("WorkerThread::run()");

    // Loop
    while (!m_abort) {
	m_mutex.lock();
        if (m_data.size() >= 2007) {
            qDebug("BLOCKED");
            condition.wait(&m_mutex);
	    m_mutex.unlock();
        }
        else {
	    qDebug("Reading from file");
            in >> r;
	    if (in.status() == QDataStream::ReadPastEnd) {
		qDebug("Datastream has read past end");
		m_abort = true;
		m_mutex.unlock();
		return;
	    }
            m_data.enqueue(r);
            m_mutex.unlock();
        }
    }
}

void WorkerThread::setFileName(QString fileName)
{
    qDebug("WorkerThread::setFileName()");

    // Close old file
    m_file.close();

    // Caution: discard all old data in QQueue
    // Worst case scenario if you don't: frame read corruption
    m_data.clear();

    // Open new file
    // XXX: toLatin1() segfaults
    m_file.setFileName(fileName);
    if (!m_file.open(QIODevice::ReadOnly)) {
    	qDebug("Cannot open file name: %s", fileName.toLatin1());
    	return;
    }

    in.setDevice(&m_file);
    extractFileHeader();
}

quint32 WorkerThread::getMagicVersion(void) const
{
    qDebug("WorkerThread::getMagicVersion()");

    return m_magicVersion;
}

quint32 WorkerThread::getProtocolVersion(void) const
{
    qDebug("WorkerThread::getProtocolVersion()");

    return m_protocolVersion;
}

float WorkerThread::getDurationInSec(void) const
{
    qDebug("WorkerThread::getDurationInSec()");

    return m_durationInSec;
}

quint32 WorkerThread::getNumOfRecords(void) const
{
    qDebug("WorkerThread::getNumOfRecords()");

    return m_numOfRecords;
}

void WorkerThread::extractFileHeader(void)
{
    qDebug("WorkerThread::extractFileHeader()");

    in >> m_magicVersion;
    //Q_ASSERT(m_magicVersion == 0xA0B0C0D0);

    in >> m_protocolVersion;
    //Q_ASSERT(m_protocolVersion == 0x1);

    in >> m_durationInSec;
    in >> m_numOfRecords;

    // qDebug() adds an extra space between items,
    // plus a new line in the last one.
    qDebug() << "Magic version =" << hex << m_magicVersion;
    qDebug() << "Protocol version =" << m_protocolVersion;
    qDebug() << "Duration in sec = " << m_durationInSec;
    qDebug() << "Number of records ="  << m_numOfRecords;
}
