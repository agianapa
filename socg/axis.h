#ifndef AXIS_H
#define AXIS_H

#include <QColor>
#include <QString>
#include <QtOpenGL>

class Axis {
public:
    Axis(QString label = "axis", QColor color = QColor(0, 255, 0), bool hasLabel = false,
         bool isVisible = true, bool isLocal = false)
    {
        m_color = color;
        m_label = label;
        m_hasLabel = hasLabel;
        m_isVisible = isVisible;
        m_isLocal = isLocal;
    };
    ~Axis() {};

    // Setters
    void setColor(QColor color) { m_color = color; }
    void setLabel(QString label)  { m_label = label; }
    void setVisible(bool isVisible) { m_isVisible = isVisible; }
    void setLocal(bool isLocal) { m_isLocal = isLocal; }

    // Getters
    const QColor *getColor(void) const { return &m_color; }
    QString getLabel(void) const {return m_label; }
    bool hasLabel(void) const { return m_hasLabel; }
    bool isVisible(void) const { return m_isVisible; }
    bool isLocal(void) const { return m_isLocal; }

    // Draw
    void draw(float x0, float y0, float z0, float x1, float y1, float z1) const
    {
        if (!m_isVisible)
            return;

        // XXX: should be tunable
        glLineWidth(1);
        glEnable(GL_LINE_SMOOTH);

        glBegin(GL_LINES);
            glColor3f(m_color.redF(), m_color.greenF(), m_color.blueF());
            glVertex3f(x0, y0, z0);
            glVertex3f(x1, y1, z1);
        glEnd();

        // Restore color and line width
        glColor3f(1.0f, 1.0f, 1.0f);
        glDisable(GL_LINE_SMOOTH);
        glLineWidth(1);
    }
private:
    QColor m_color;
    QString m_label;
    bool m_hasLabel;
    bool m_isVisible;
    bool m_isLocal;
};

#endif    // AXIS_H
