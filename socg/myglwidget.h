#ifndef MYGLWIDGET_H
#define MYGLWIDGET_H

#include <iostream>
#include <QGLWidget>
#include <QMutex>
#include <QTimer>

#include "axis.h"
#include "camera.h"
#include "coordsAndAngles.h"
#include "sphere.h"

typedef enum axis_t { AXIS_X, AXIS_Y, AXIS_Z };

class WorkerThread;

class MyGLWidget : public QGLWidget
{
    Q_OBJECT

public:
    // Non-default arguments must come first
    MyGLWidget(Axis *pAxisX = 0, Axis *pAxisY = 0, Axis *pAxisZ = 0,
               Camera *pCamera = 0, Sphere *pSphere = 0, QWidget *parent = 0);
    ~MyGLWidget();

    // Setters
    void setAxis(axis_t axis, const Axis *pAxis);
    void setCamera(const Camera *pCamera);
    void setSphere(const Sphere *pShere);
    void setCoordsAndAngles(const CoordsAndAngles *pCoordsAndAngles);
protected:
    void initializeGL();
    void paintGL();
    void resizeGL(int width, int height);

private:
    // Functions
    void drawAxis(void);
    void drawSphere(void);

    // Sphere coordinations and orientation
    const CoordsAndAngles *m_pCoordsAndAngles;

    const Axis *m_pAxisX;
    const Axis *m_pAxisY;
    const Axis *m_pAxisZ;

    const Camera *m_pCamera;
    const Sphere *m_pSphere;
};

#endif    // MYGLWIDGET_H
